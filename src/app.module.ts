import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { NewsModule } from './news/news.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    NewsModule,
    MongooseModule.forRoot(
      'mongodb://reign:reign1234@mongodb:27017/reign',
      { useNewUrlParser: true, useUnifiedTopology: true },
    ),
    ScheduleModule.forRoot(),
    AuthModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
