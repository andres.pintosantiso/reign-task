import { IsOptional, IsString, IsArray, IsEnum } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Transform } from 'class-transformer';
import { PaginationDto } from '../../shared/dto/pagination.dto';
import { MONTHS_NUMBER } from '../../shared/constants/date-constants';

export class NewsFiltersDto extends PaginationDto {
  @IsOptional()
  @IsArray()
  @ApiProperty({ type: [String], required: false })
  tags?: string[];

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  title?: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  author?: string;

  @IsOptional()
  @IsEnum(MONTHS_NUMBER)
  @Transform(({ value }) => value.toLowerCase())
  @ApiProperty({ required: false, enum: MONTHS_NUMBER })
  month?: string;
}
