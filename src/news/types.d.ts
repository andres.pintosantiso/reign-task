export interface IGetAllNews {
  news: News[];
  limit: number;
  offset: number;
  totalHits: number;
}
