import { Model } from 'mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { HttpModule } from '@nestjs/axios';
import { News, NewsDocument } from './schemas/news.schema';
import { NewsService } from './news.service';

describe('NewsService', () => {
  let service: NewsService;
  let model: Model<NewsDocument>;

  afterAll(() => {
    jest.resetAllMocks();
  });

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        NewsService,
        {
          provide: getModelToken(News.name),
          useValue: {
            new: jest.fn().mockResolvedValue(3),
            constructor: jest.fn().mockResolvedValue(4),
            aggregate: jest.fn().mockReturnThis(),
            create: jest.fn(),
            remove: jest.fn(),
            exec: jest.fn(),
          },
        },
      ],
      imports: [HttpModule],
    }).compile();

    service = module.get<NewsService>(NewsService);
    model = module.get<Model<NewsDocument>>(getModelToken(News.name));
  });

  it('service and model should be defined', () => {
    expect(service).toBeDefined();
    expect(model).toBeDefined();
  });

  it('should call Agreggate with month 12 and author "test"', async () => {
    service.findAll({ author: 'test', month: 'december' });
    expect(model.aggregate).toBeCalledWith([
      {
        $addFields: {
          month: { $month: '$created_at' },
        },
      },
      { $match: { deleted_at: null, author: 'test', month: 12 } },
      {
        $facet: {
          data: [{ $match: {} }, { $skip: 0 }, { $limit: 5 }],
          count: [{ $count: 'count' }],
        },
      },
      { $unwind: '$count' },
    ]);
  });

  it('findAll should return empty news and default limit-offset', async () => {
    jest.spyOn(model, 'aggregate').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce([]),
    } as any);
    const result = await service.findAll();
    expect(result).toEqual({ limit: 5, news: [], offset: 0, totalHits: 0 });
  });

  it('findAll should return fetch news and totalHits', async () => {
    jest.spyOn(model, 'aggregate').mockReturnValue({
      exec: jest.fn().mockResolvedValueOnce([
        {
          data: [
            {
              created_at: '2021-12-11T02:22:22.000Z',
              title: null,
              url: null,
              author: 'rglullis',
              points: null,
              story_text: null,
              comment_text:
                'Would you like to check how many people are running ETH2.0 validators [0] vs how many people are running Handshake nodes?<p>[0]: <a href="https:&#x2F;&#x2F;launchpad.ethereum.org&#x2F;en&#x2F;" rel="nofollow">https:&#x2F;&#x2F;launchpad.ethereum.org&#x2F;en&#x2F;</a>',
              num_comments: null,
              story_id: 29513547,
              story_title:
                'Show HN: Decentralized, Mutable, Serverless Torrent Swarm Websites',
              story_url: 'https://github.com/publiusfederalist/federalist',
              parent_id: 29517574,
              created_at_i: 1639189342,
              _tags: ['comment', 'author_rglullis', 'story_29513547'],
              objectID: '29517822',
            },
          ],
          count: { count: 1 },
        },
      ]),
    } as any);
    const result = await service.findAll();
    expect(result).toEqual({
      limit: 5,
      news: [
        {
          created_at: '2021-12-11T02:22:22.000Z',
          title: null,
          url: null,
          author: 'rglullis',
          points: null,
          story_text: null,
          comment_text:
            'Would you like to check how many people are running ETH2.0 validators [0] vs how many people are running Handshake nodes?<p>[0]: <a href="https:&#x2F;&#x2F;launchpad.ethereum.org&#x2F;en&#x2F;" rel="nofollow">https:&#x2F;&#x2F;launchpad.ethereum.org&#x2F;en&#x2F;</a>',
          num_comments: null,
          story_id: 29513547,
          story_title:
            'Show HN: Decentralized, Mutable, Serverless Torrent Swarm Websites',
          story_url: 'https://github.com/publiusfederalist/federalist',
          parent_id: 29517574,
          created_at_i: 1639189342,
          _tags: ['comment', 'author_rglullis', 'story_29513547'],
          objectID: '29517822',
        },
      ],
      offset: 0,
      totalHits: 1,
    });
  });
});
