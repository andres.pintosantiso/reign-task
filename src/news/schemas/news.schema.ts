import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NewsDocument = News & Document;

@Schema()
export class News {
  @Prop()
  title: string;

  @Prop({ unique: true })
  objectID: string;

  @Prop()
  url: string;

  @Prop()
  author: string;

  @Prop()
  points: string;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  num_comments: number;

  @Prop()
  story_id: number;

  @Prop()
  story_title: string;

  @Prop([String])
  _tags: string[];

  @Prop()
  story_url: string;

  @Prop()
  parent_id: number;

  @Prop()
  created_at_i: number;

  @Prop()
  created_at: Date;

  @Prop({ default: null })
  deleted_at: Date;
}

export const NewsSchema = SchemaFactory.createForClass(News);
