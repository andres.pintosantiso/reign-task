import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { getModelToken } from '@nestjs/mongoose';
import { News } from './schemas/news.schema';

const mockRepository = {
  find() {
    return {};
  },
};
describe('NewsController', () => {
  let newsController: NewsController;
  let newsService: NewsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [NewsController],
      providers: [
        NewsService,
        { provide: getModelToken(News.name), useValue: mockRepository },
      ],
      imports: [HttpModule],
    }).compile();
    newsController = module.get(NewsController);
    newsService = module.get(NewsService);
  });

  it('controller and service should be defined', async () => {
    expect(newsController).toBeDefined();
    expect(newsService).toBeDefined();
  });
});
