import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { AxiosResponse } from 'axios';
import { InjectModel } from '@nestjs/mongoose';
import { Cron, CronExpression } from '@nestjs/schedule';
import { Model } from 'mongoose';
import { lastValueFrom } from 'rxjs';
import { News, NewsDocument } from './schemas/news.schema';
import { NewsFiltersDto } from './dto/news-filters.dto';
import { IGetAllNews } from './types';
import { MONTHS_NUMBER } from '../shared/constants/date-constants';

@Injectable()
export class NewsService {
  constructor(
    @InjectModel(News.name) private newsModel: Model<NewsDocument>,
    private httpService: HttpService,
  ) {}

  async findAll(filters?: NewsFiltersDto): Promise<IGetAllNews> {
    const { limit = 5, offset = 0, tags, title, author, month } = filters || {};

    const query = {
      deleted_at: null,
      ...(title && { title }),
      ...(author && { author }),
      ...(tags && { _tags: { $all: tags } }),
      ...(month && { month: MONTHS_NUMBER[month] }),
    };

    const news = await this.newsModel
      .aggregate<{ data: News[]; count: { count: number } }>([
        {
          $addFields: {
            month: { $month: '$created_at' },
          },
        },
        { $match: query },
        {
          $facet: {
            data: [{ $match: {} }, { $skip: offset }, { $limit: limit }],
            count: [{ $count: 'count' }],
          },
        },
        { $unwind: '$count' },
      ])
      .exec();

    if (!news || news.length === 0) {
      return {
        news: [],
        limit,
        offset,
        totalHits: 0,
      };
    }

    return {
      news: news[0].data,
      limit,
      offset,
      totalHits: news[0].count.count,
    };
  }

  remove(id: string): void {
    this.newsModel.updateOne({ objectID: id }, { deleted_at: Date.now() });
    return;
  }

  async fetchNews(): Promise<News[]> {
    const fetchData: AxiosResponse<{ status: number; hits: News[] }> =
      await lastValueFrom(
        this.httpService.get(
          'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
        ),
      );
    return fetchData.data.hits;
  }

  @Cron(CronExpression.EVERY_HOUR)
  async feedNews(): Promise<void> {
    const news = await this.fetchNews();
    news.forEach(async (singleNews) => {
      this.newsModel.findOneAndUpdate(
        { objectID: singleNews.objectID },
        singleNews,
        { upsert: true, setDefaultsOnInsert: true },
      );
    });
  }
}
