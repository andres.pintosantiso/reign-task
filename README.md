# reign-task

Task for application

## Requirements

Docker, NodeJS and Postman (or other api tester) installed.

## Installation

If you want to initialize the app all dockerized just run:
### `docker composer up dev`.

If you want to initialize the app from npm and DB dockerized you could run:
### `docker composer up mongodb`.
### `npm run start:dev`.

## How to get a token

To generate an access_token go to:
### `Get request to: http://localhost:3001/auth`

Other routes are protected, you need to send a header named "Authorization" with a value "bearer put_here_your_access_token_from_above_url"

## You can test this API with swagger!

Just make sure you request your authorization token from auth route and send it in all the routes  with the button "Authorization" on swagger.

To Access this api docs  go to:
### `Get request to: http://localhost:3001/api/docs`

## Routes

To populate the database for the first time (in postman) or manually instead of waiting for every hour cronjob:

### `Post request to: http://localhost:3001/news`

To Remove a new and hide it (this will persist even if you rerun the app, except if you delete localFolder)

### `Delete request to: http://localhost:3001/news/:id`


To get news 

### `Delete request to: http://localhost:3001/news`

You can send query strings with author, month(only month name, ex: december), title and tags. If you go to api docs you can see all the query possibilities you have.



